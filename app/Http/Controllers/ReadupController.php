<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;

use App\Http\Resources\Article as ArticleResource;
use App\Http\Resources\Category as CategoryResource;

class ReadupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('readup');
    }

    public function show(Request $request) {

        if($request->category_id) {

            switch($request->category_type) { 
                case 0; 
                case 3; 
                        $category = 'category'; 
                break;
                case 1: $category = 'sub_category';
                break; 
                case 2: $category = 'extra_category';
                break; 
            }

            $articles = Article::where($category, $request->category_id)->orderBy('popularity', 'DESC')->paginate(18);

        } else $articles = Article::orderBy('popularity', 'DESC')->paginate(3);

        return new ArticleResource($articles);
    }
}
