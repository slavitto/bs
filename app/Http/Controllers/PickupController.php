<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class PickupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pickup');
    }

    public function show(Request $request) {
        if($request->category_id) {
            return Company::where('sphere', $request->category_id)->orderBy('updated_at', 'desc')->paginate(9);

        } else 
        return Company::orderBy('updated_at', 'desc')->paginate(9);
    }
}
