<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Company as CompanyResource;
use JavaScript;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    // function __construct() {
    //     global $authId;
    //     $this->authId = Auth::id();
    // }

    public function index($id = 0)
    {   

        $company = Company::where('id', $id)->first();
    	$auth = Auth::User();

    	if(!$id && !$auth) {

            return view('auth/register');

        } else if(!$company && $id != 0 || $auth && $id != 0 && !$company || !is_numeric($id)) {

    		return view('welcome');

    	} else if(!$id && $auth) {

            JavaScript::put([
            'company_id' => 0,
            'editable' => true,
            'contact_id' => $auth ? $auth->id : ''
            ]);
            
            return view('company', [ 'edit' => true ]);

        } else {

	        if($auth && $company && $id !== 0) {

    			$editable = $auth->id == $company->contact_id || $auth->id == 1 ? true : false;

	    	} else {
                
				$editable = false; 
	    	}

	        JavaScript::put([
	        'company_id' => $id,
	        'editable' => $editable,
            'contact_id' => $auth ? $auth->id : ''
	        ]);

	        return view('company', [ 'edit' => $editable ]);
    	
        } 
    }

    public function show($id) 
    {   
        if($id == 0) $id = Company::latest()->first()->id;
        $company = Company::where('id', $id)->first();
        return new CompanyResource($company);
    }

    public function update(Request $request)
    {

        if($request->input('delete')) {
            if(Company::destroy($request->input('id'))) {
                echo 'deleted'; 
            } else echo 'error';
            
        }

        if($request->input('id') != 0) {
        	$company = Company::findOrFail($request->input('id'));
        } else $company = new Company();

        // $keys = array_keys($request->input());

        // foreach ($keys as $key) {

        //    $val = $request->input($key); 
        //    if($val != 0 && $val != '') {

        //         $company->$key = $request->input($key);
        //     }
        // }

        // $company->id = $r equest->input('id');
        
        $company->contact_id = $request->input('contact_id');
        $company->img_url = $request->input('img_url');
        $company->title = $request->input('title');
        $company->description = $request->input('description');
        $company->about = $request->input('about');
        $company->sphere = $request->input('sphere');
        $company->price_category = $request->input('price_category');
        $company->phone = $request->input('phone');
        $company->www = $request->input('www');
        $company->metro = $request->input('metro');
        $company->city = $request->input('city');
        $company->url = '/'.Str::slug($request->input('title')).'.html';
        $company->working_hrs = $request->input('working_hrs');
        $company->address = $request->input('address');
        $company->email = $request->input('email');
        $company->agreement = $request->input('agreement');

        if($company->save()) {
            return new CompanyResource($company);
        }
    }
}

