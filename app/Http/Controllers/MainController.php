<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use App\Category;
use App\Article;
use App\Company;
use JavaScript;
use Redirect;
use Request;
use Auth;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index($path1 = '', $path2 = '', $path3 = '')
    {   
        $url = $path2 && $path1 === 'soveti' ? $path2 : $path1;
        $auth = Auth::User();

        if($url) {

            $category = Category::where('slug', $url)->first();

            if($category) {
                JavaScript::put([
                'category_id' => $category->id,
                'category_name' => $category->name,
                'category_type' => $category->type
                ]);

                return view($category->parent_id === 7 && $path1 !== 'soveti' ? 'pickup' : 'readup');

            } else {
                
                if($path2) $url .= '/'.$path2;
                if($path3) $url .= '/'.$path3;

                $article = Article::where('url', $url)->first();
                
                if($article === null) $article = Article::where('url', $url.'.html')->first(); /// crunch for queres w/o "html" ///

                if($article !== null) { 

                    if($auth && $article) {

                        $editable = $auth->id == $article->author_id ? true : false;

                    } else {
                        $editable = false; 
                    }

                    JavaScript::put([
                        'article_id' => $article->id,
                        'editable' => $editable,
                        'url' => $url
                        ]);
                    
                    return view('article', [ 'edit' => false ]);

                } else {

                    $company = Company::where('url', '/'.$url)->first();

                    if($company) {

                        if($auth && $company) {

                            $editable = $auth->id == $company->contact_id || $auth->id == 1 ? true : false;

                        } else {

                            $editable = false; 
                        }

                        JavaScript::put([
                        'company_id' => $company->id,
                        'url' => $url,
                        'contact_id' => $auth ? $auth->id : false,
                        'editable' => $editable
                        ]);
                    
                        return view('company', [ 'edit' => false ]);

                    } else return Redirect::to('/'); // error 404 page!!
                }
            }     
        }
    }
}
