<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Article as ArticleResource;
use JavaScript;
use Illuminate\Support\Str;


class ArticleController extends Controller
{

    public function show($id = 0) 
    {   
        if($id == 0) $id = Article::latest()->first()->id;
        $article = Article::where('id', $id)->first();
        return new ArticleResource($article);
    }

    public function edit($id = 0)
    {   
    	$article = $id ? Article::where('id', $id)->first() : (object)[ 'author_id' => false ];
    	$auth = Auth::User();

        if(!$auth || !in_array($auth->id, [1, 2, 3]) && !$id || $auth->id != $article->author_id && !in_array($auth->id, [1, 2, 3])) {
            return view('welcome');
        } else {
            
	        JavaScript::put([
	        'id' => $id,
            'author_id' => $auth->id
	        ]);

	        return view('article', [ 'edit' => true ]);
    	}	
    }

    public function update(Request $request)
    {

        if($request->input('id') != 0) {
        	$article = Article::findOrFail($request->input('id'));

        } else $article = new article();


        $article->author_id = $request->input('author_id');
        $article->title = $request->input('title');
        $article->url = 'story/'.Str::slug($request->input('title')).'.html';
        $article->thumb = $request->input('thumb');
        $article->description = $request->input('description');
        $article->body = $request->input('body');
        $article->category = $request->input('category');
        $article->sub_category = $request->input('sub_category');
        $article->extra_category = $request->input('extra_category');
        $article->guest_count = $request->input('guest_count');
        $article->place = $request->input('place');
        $article->players = $request->input('players');
        $article->from_who = $request->input('from_who');
        $article->to_whom = $request->input('to_whom');
        $article->genre = $request->input('genre');


        if($article->save()) {

            return new ArticleResource($article);
        }
    }
}

