<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Resources\Media as MediaResource;
use Carbon\Carbon;
use Image;
use JavaScript;

class LookupController extends Controller
{

    public function index() {
        
        return view('lookup');
    }

}