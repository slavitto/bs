<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Resources\Media as MediaResource;
use Carbon\Carbon;
use Image;
use JavaScript;
use Illuminate\Pagination\Paginator;

class MediaController extends Controller
{

    public function index($owner, $path, $media) {

        $media = Media::where('id', $media)->first();

        JavaScript::put([
            'media_id' => $media['id'],
            'album_id' => $media['album_id'],
            'user_id' => $media['user_id'],
            'editable' => Auth::id() == $media['user_id'] || Auth::id() == 1 ? 1 : 0
        ]);

        return view('media', [ 'edit' => false ]);
    }

    public function edit($id)
    {   
        if(Auth::User()) {

            JavaScript::put([
                'id' => $id,
                'editable' => true
            ]);

            return view('media', [ 'edit' => true ]);

        } else {
            
            return view('auth/login');
        }
    }


    public function show($id) 
    {
        $media = Media::where('id', $id)->first();
        
        return new MediaResource($media);
    }

    public function showAll(Request $request) 
    {
        if($request->album_id) {

            $media = Media::where('album_id', $request->album_id)->get();

        } else {

            $media = Media::inRandomOrder()->paginate(36);
        }

        if($media) return $media;
    }

    public function upload(Request $request) {

        $validator = Validator::make($request->all(), [
            'image' => 'required|image64:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {

            return response()->json(['errors'=>$validator->errors()]);
        } else {

            $imageData = $request->get('image');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            if($request->path) $fileName = $request->path.$fileName;

            $fileUrl = public_path('images/').$fileName;

            Image::make($request->get('image'))->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($fileUrl);

            return response()->json([ 'error' => false, 'file' => $fileName ]);
        }
    }

    public function update(Request $request)
    {

        $media = Media::where('id', $request->input('id'))->first();
        if(!$media) $media = new Media();

        // $keys = array_keys($request->input());

        // foreach ($keys as $key) {

        //    $val = $request->input($key); 
        //    if($val != 0 && $val != '') {

        //         $media->$key = $request->input($key);
        //     }
        // }

        // $media->id = $request->input('id');
        $media->url = $request->input('url');
        $media->title = $request->input('title');
        $media->description = $request->input('description');
        $media->gallery_id = $request->input('gallery_id');
        $media->album_id = $request->input('album_id');
        $media->media_type = $request->input('media_type');
        $media->user_id = $request->input('user_id');
        // $media->country = $request->input('country');


        if($media->save()) {
            return new MediaResource($media);
        }
    }
}