<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use App\Category;

use App\Http\Resources\Category as CategoryResource;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = false)
    {
       // 
    }

    public function showAll($parent_id = false) {

        if($parent_id) {
            $category = Category::where('parent_id', $parent_id)->get();

        } else $category = Category::get();

        return new CategoryResource($category);
    }

    public function show($id) {

        $category = Category::where('id', $id)->first();

        return new CategoryResource($category);
    }

}
