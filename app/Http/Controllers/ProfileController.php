<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Profile as ProfileResource;
use JavaScript;

class ProfileController extends Controller
{

    public function index($login)
    {   
        $user = User::where('login', $login)->first();

        if(!$user) return view('welcome');

            JavaScript::put([
            'login' => $login,
            'editable' => Auth::id() === $user->id || Auth::id() === 1 ? true : false
            ]);

            return view('profile',[ 'edit' => false ]);
    }

    public function edit($login)
    {   
        $user = User::where('login', $login)->first();

        if(!$user) return view('welcome');

        if(Auth::id() === $user->id || Auth::id() === 1) {

            JavaScript::put([
            'login' => $login
            ]);

            return view('profile',[ 'edit' => true ]);

        } else {
            
            return view('auth/login');
        }
    }

    public function show($login) 
    {
        $profile = User::where('login', $login)->first();
        return new ProfileResource($profile);
    }

    public function update(Request $request)
    {

        $profile = User::findOrFail($request->input('id'));

        // $keys = array_keys($request->input());

        // foreach ($keys as $key) {

        //    $val = $request->input($key); 
        //    if($val != 0 && $val != '') {

        //         $profile->$key = $request->input($key);
        //     }
        // }

        // $profile->id = $request->input('id');
        $profile->img_url = $request->input('img_url');
        $profile->name = $request->input('name');
        $profile->status = $request->input('status');
        $profile->about = $request->input('about');
        $profile->interests_list = $request->input('interests_list');
        $profile->gender = $request->input('gender');
        $profile->date_of_birth = $request->input('date_of_birth');
        $profile->martial_status = $request->input('martial_status');
        $profile->date_wedding = $request->input('date_wedding');
        // $profile->country = $request->input('country');
        $profile->city = $request->input('city');


        if($profile->save()) {
            return new ProfileResource($profile);
        }
    }
}