<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Company;
use App\User;
use App\Media;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Article as ArticleResource;
use App\Http\Resources\Company as CompanyResource;

class ParseController extends Controller
{

    private $interests = ["кино", "музыка", "ролики", "пение", "флирт", "hip pop", "mc", "r n b", "вермут", "вино", "виски", "коньяк", "ликер", "книги", "работа", "ведущий", "свадьба", "тамада", "btl", "coffee", "dj", "djing", "pr", "promotion", "tv", "артисты", "артхаус", "арфа", "арфист", "бизнес", "брак", "девушки", "деньги", "дети", "джаз", "дуэты", "звезды", "ивент", "клубы", "кофе", "мохито", "невеста", "отдых", "реклама", "рояль", "скрипка", "тусовки", "фильмы", "флейта", "фото", "фотошоп", "шоу", "рыбалка",  "Blu-ray", "портрет", "студия", "поэзия", "проза", "религия", "спорт", "массаж", "жемчуг", "золото", "кулоны", "серьги", "танцы", "стихи", "кино", "природа", "индия", "еда", "подарки", "лирика", "секс", "кошки", "пазлы", "кино", "мюзикл", "дизайн", "туризм", "верстка", "любовь", "мужчины", "платья", "свадьбы", "монтаж", "ит", "походы", "ребенок", "муж", "общение", "макияж", "салюты", "певица", "небо", "друзья", "фитнес", "декор", "кино", "семья", "кино", "закон", "право", "красоту", "эмоции", "истра", "вязание", "охота", "декупаж", "кино", "графика"];

    public function index($dir = '', $subdir = '')
    {   
        if(!Auth::id()) return view('welcome');

        $files = array();
        $path = '/Volumes/Macintosh HD/Users/slavitto/Documents/_projects/_www/budetsvadba.ru/company/'.$dir.'/'.$subdir;

        if($handle = opendir($path)) {
                while (false !== ($entry = readdir($handle))) {
                    array_push($files, $entry);
                }
            closedir($handle);
            $files = array_splice($files, 1);
            // for($k = 1; $k < count($files); $k++) {
            for($k = 1000; $k < 1418; $k++) { //ОБНОВИТЬ КОМПАНИИ DESCRIPTION

                // if (strpos($files[$k], '_filter') !== false) continue; 

                $html = file_get_contents($path.'/'.$files[$k]);

                //FOR TOASTS
                // $html = file_get_contents('/Volumes/Macintosh HD/Users/slavitto/Documents/_projects/_www/budetsvadba.ru/'.$dir); 

                // $cat_block = $this->btw($html, 'item_img', '/td>');
                // $cats = explode('html', $cat_block);

                // $cat_id = $this->cats(preg_replace('/["a-z\/&;=_.<>-]/', '', @$cats[2]), 1);
                // $sub_cat_id = $this->cats(preg_replace('/["a-z\/&;=_.<>]/', '', @$cats[3]), 2);


                // if(@$cats[4] && @$cats[4] != NULL) {
                //     $extra_cat_id = $this->cats(preg_replace('/["a-z\/&;=_.<>]/', '',@$cats[4]), 3);
                // } else if($sub_cat_id == NULL) { 
                //     $extra_cat_id = $this->cats(preg_replace('/["a-z\/&;=_.<>]/', '',@$cats[3]), 3);
                // } else $extra_cat_id = false;

                //-------ARTICLE
                // $data = array (
                //     'author_id' => 1,
                //     'title' => $this->btw($html, '<h6>', '</h6>'),
                //     'url' => $files[$k],
                //     'category' => $cat_id,
                //     'sub_category' => $sub_cat_id,
                //     'description' => preg_replace('/[«»]/', '"', $this->btw($html, '<meta name="Description" content="', '/>')),
                //     'extra_category' => $extra_cat_id,
                //     'thumb' => $this->btw($html, '/images/story/', '"'),
                //     'body' => $this->btw($this->btw($html, '</h6>', 'd>'), '/a>', '</t')
                // );

                //------BLOG
                // $data = array (
                //     'author_id' => 1,
                //     'title' => $this->btw($html, '<h6>', '</h6>'),
                //     'url' => $files[$k],
                //     'category' => 10,
                //     // 'description' => preg_replace('/[«»]/', '"', $this->btw($html, '<meta name="Description" content="', '/>')),
                //     'body' => preg_replace('/[«»]/', '"', $this->btw($html, '</h6>', '<hr')),
                //     'tags' => $this->tags($this->btw($html, 'a_11">', '</p>'))
                // );

                //-------CONTESTS
                // $box_data = explode('div_white_grey', $html);
                // $data = array (
                //     'author_id' => 1,
                //     'title' => $this->btw($html, '<h6>', '</h6>'),
                //     'url' => $files[$k],
                //     'category' => 1,
                //     'sub_category' => 11,
                //     'description' => preg_replace('/[«»]/', '', $this->btw($html, '<meta name="Description" content="', '/>')),
                //     'body' => preg_replace('/[«»]/', '"', $this->btw($html, '</h6>', '</td>')),
                //     'guest_count' => $this->boxData($this->btw($box_data[1], 'span>', '</span')),
                //     'players' => $this->boxData($this->btw($box_data[2], 'p>', '</'), 1)
                // );

                // ---- SCENARIO
                // $box_data = explode('div_white_grey', $html);
                // $data = array (
                //     'author_id' => 1,
                //     'title' => $this->btw($html, '<h6>', '</h6>'),
                //     'url' => $files[$k],
                //     'category' => 5,
                //     'sub_category' => 26,
                //     'extra_category' => $extra_cat_id,
                //     'description' => preg_replace('/[«»]/', '', $this->btw($html, '<meta name="Description" content="', '/>')),
                //     'body' => preg_replace('/[«»]/', '"', $this->btw($html, '</h6>', '</td>')),
                //     'guest_count' => $this->boxData($this->btw($box_data[1], 'span>', '</span')),
                //     'place' => $this->boxData($this->btw($box_data[2], 'p>', '</'), 2)
                // );

                // ---- TOASTS
                // $body = preg_replace('/[«»]/', '"', $this->btw($html, '</h6>', '</td>'));
                // $toasts = explode('<div class="line_hr"></div>', $body);

                // foreach($toasts as $html) {

                //     $box_data = explode('bg_grey">', $html);

                //     $data = array (
                //         'author_id' => 1,
                //     //     'title' => $this->btw($html, '<h6>', '</h6>'),
                //     //     'url' => $files[$k],
                //         'category' => 4,
                //         'sub_category' => 24,
                //         'to_whom' => $this->boxData($this->btw(@$box_data[2], '">', '<'),1),
                //         'genre' => $this->boxData($this->btw(@$box_data[3], '">', '<')),
                //         // 'genre' => $genre,
                //         'from_who' => $this->boxData($this->btw(@$box_data[1], '">', '<'),2),
                //     //     'description' => preg_replace('/[«»]/', '', $this->btw($html, '<meta name="Description" content="', '/>')),
                //         'body' => preg_replace('/[«»]/', '"', $this->btw($html, '<p style="font-size:12px;">', '</p>')),
                //     //     'guest_count' => $this->boxData($this->btw($box_data[1], 'span>', '</span')),
                //     //     'place' => $this->boxData($this->btw($box_data[2], 'p>', '</'), 2)
                //     );

                //     $this->update($data);
                //     // echo mb_convert_encoding($this->recode($this->btw(@$box_data[3], '">', '<')), 'utf-8');
                // }

                //COMPANIES
                // $oval_data = $this->btw($html, 'oval_right">', '</div');

                // if($oval_data !== '') {

                //     $data = array (
                //         // 'contact_id' => 0,
                //         'title' => $this->btw($html, '<h6>', '</h6>'),
                //         'url' => $files[$k],
                //         'extra_category' => $extra_cat_id,
                //         // 'description' => preg_replace('/[«»]/', '', $this->btw($html, '<meta name="Description" content="', '/>')),
                //         // 'about' => preg_replace('/[«»]/', '"', $this->btw($html, '</h3>', '<p style="clear: both;">')),
                //         'description' => substr($this->btw($html, '<meta name="Description" content="', '"/>'), 0, 511),
                //         'about' => $this->btw($html, '</h3>', '<p style="clear: both;">'),
                //         'thumb' => $this->btw($html, 'images/company/', '"'),
                //         'phone' => $this->phone($oval_data),
                //         'www' => $this->btw($oval_data, 'www.', '</') != '' ? $this->btw($oval_data, 'www.', '</') : $this->btw($oval_data, 'http://', '</'),
                //         'city' => $this->cities($this->oval_data($this->btw($oval_data, 'City', 'a>'))),
                //         'metro' => $this->metro($this->oval_data($this->btw($oval_data, 'Metro', 'a>'))),
                //         'price_category' => $this->price($oval_data),
                //         'address' => $this->addr($oval_data)

                //     );

                //     $this->update($data);

                //ZAGS
                // $oval_data = $this->btw($html, 'oval_right">', '</div');
                // $oval_arr = explode('<span', $oval_data);    

                // if($oval_data !== '') {

                //     $data = array (
                //         // 'contact_id' => 0,
                //         'title' => $this->btw($html, '<h6>', '</h6>'),
                //         'url' => 'zags/Show/'.$files[$k],
                //         'extra_category' => 75,
                //         // 'description' => preg_replace('/[«»]/', '', $this->btw($html, '<meta name="Description" content="', '/>')),
                //         // 'about' => preg_replace('/[«»]/', '"', $this->btw($html, '</h3>', '<p style="clear: both;">')),
                //         // 'description' => substr($this->btw($html, '<meta name="Description" content="', '"/>'), 0, 511),
                //         'about' => $this->btw($html, '<div class="mapit">', '</div>'),
                //         // 'thumb' => $this->btw($html, 'images/company/', '"'),
                //         'phone' => $this->phone($oval_data),
                //         // 'www' => $this->btw($oval_data, 'www.', '</') != '' ? $this->btw($oval_data, 'www.', '</') : $this->btw($oval_data, 'http://', '</'),
                //         'city' => $this->cities($oval_arr),
                //         // 'metro' => $this->metro($oval_arr),
                //         // 'price_category' => $this->price($oval_data),
                //         'address' => $this->addr($oval_data)

                //     );

                //     $this->update($data);

                // }

                //USERS
                $oval_data = $this->btw($html, 'oval_right">', '</div');
                $oval_arr = explode('<span', $oval_data);    

                if($html) {

                    $html_recoded = mb_convert_encoding($this->recode($html), 'utf-8');

                    $data = array (
                        // 'contact_id' => 0,
                        // 'login' => $this->btw($this->btw($html, 'height="61"', '/p>'), 'html">','<'),
                        // 'url' => $files[$k],
                        'url' => $this->btw($this->btw($html, 'big_f">', '</a'), 'img src="../../..', '"'),
                        'album_id' => $this->btw($this->btw($html, 'big_f', 'big_f'), 'wedding_photoreports/', '/'),
                        'album_name' => $this->btw($this->btw($html_recoded, 'h6>', '</h6'), '>', '<'),
                        'description' => $this->btw($this->btw($html_recoded, 'h6>', '/h6'), '|', '<'),
                        'created_at' => $this->btw($html, 'data">', '<'),
                        'user_login' => $this->btw($this->btw($html, 'top_mini', '<img'), '../../..', '"'),
                        // 'name' => preg_match('#ФИО:(.+)<#u', $html_recoded, $out) ? $this->preg($out) : '',
                        // 'gender' => preg_match('#Пол:(.+)<#u', $html_recoded, $out) ? $this->preg($out) : '',
                        // 'date_of_birth' => preg_match('#рожд.:(.+)<#u', $html_recoded, $out) ? $this->preg($out) : '',
                        // 'city' => preg_match('#>(.+)\(#u', $this->btw($html_recoded, 'FromCity', ')'), $out) ? $this->preg($out) : '',
                        // 'martial_status' => preg_match('#>(.+)\(#u', $this->btw($html_recoded, 'ForStatus', ')'), $out) ? $this->preg($out) : '',
                        // 'date_wedding' => preg_match('#свадьбы:(.+)<#u', $html_recoded, $out) ? $this->preg($out) : '',
                        // 'company' => $this->btw($this->btw($html_recoded, 'font_13', '/a'), '../', '">'),
                        
                        // 'interests' => $this->actionInterests(strip_tags($this->btw($html_recoded, 'box_text">', '/div'))),

                        // 'extra_category' => 75,
                        // 'description' => preg_replace('/[«»]/', '', $this->btw($html, '<meta name="Description" content="', '/>')),
                        // 'about' => preg_replace('/[«»]/', '"', $this->btw($html, '</h3>', '<p style="clear: both;">')),
                        // 'description' => substr($this->btw($html, '<meta name="Description" content="', '"/>'), 0, 511),
                        // 'about' => $this->btw($html, '<div class="mapit">', '</div>'),
                        // 'phone' => $this->phone($oval_data),
                        // 'www' => $this->btw($oval_data, 'www.', '</') != '' ? $this->btw($oval_data, 'www.', '</') : $this->btw($oval_data, 'http://', '</'),
                        // 'city' => $this->cities($oval_arr),
                        // 'metro' => $this->metro($oval_arr),
                        // 'price_category' => $this->price($oval_data),
                        // 'address' => $this->addr($oval_data)

                    );
                    // if(preg_match('/gif/', $data['img_url']) && !$data['company']) continue;
                    $this->update($data);
                    // echo '"'.$data['img_url'].'"<br>';

                }
                
            }
        }
    }


    private function btw($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    private function actionInterests($data) {
        $res = '';
        $items = explode(',', $data);
        foreach($items as $item) {
            if($i = array_search(substr($item, 1), $this->interests)) $res .= ';'.$i;
        }
        return $res;
    }

    private function oval_data($txt) {

        return $this->btw($txt, '>', '</');
    }

    private function addr($oval_data) {

        $data = explode('data', $oval_data);

        // if(isset($data[5])) {
        //     return $this->btw($data[3], 'span>', '</');
        // }
if(isset($data[2]))
        return $this->btw($data[2], 'span>', '</');

    }

    private function price($oval_data) {

        $data_arr = explode('data', $oval_data);

        if(isset($data_arr[7])) {

            $data_price_recoded = strip_tags(preg_replace('/[^0123456789-а-ячстюрцущфхыьА-Я\s.]/', '', mb_convert_encoding($this->recode($this->btw($data_arr[7], 'span>', '</')), 'utf-8')));

            $price_cat = [
            'до 20 т.руб.','до 60 т.руб.','свыше 60 т.р.', 
            'эконом','стандарт','VIP', 
            'до 5 т.руб.','до 10 т.руб.','свыше 10 т.р.', 
            'свыше 20 т.р.', 
            'до 2 т.р. с человека','свыше 2 тыс.руб.', 
            'до 1 т.р. за 1кг','до 3 т.руб.','свыше 3 т.р.', 
            'до 2 т.р.', 'до 10 тыс.руб.', 
            'до 2 т.р. в час','до 5 т.руб.','свыше 5 т.р.', 
            'до 7 т.р. в сутки','свыше 7 т.р.', 
            'до 7 т.руб.','до 20 т.руб.','свыше 20 т.р.', 
            'до 1 т.р. за занятие','до 2 т.руб.', 
            'до  30 т.руб.','свыше 30 т.р.', 
            'до 100р.','свыше 100р.'];

            $price_cat_id = array_search($data_price_recoded, $price_cat);

            // echo $data_price_recoded.'--'.$price_cat[0].'<br>';

            switch($price_cat_id) {
                case 0; case 3; case 6; case 12; case 15; case 17; case 20; case 22; case 25;
                return 1;
                break;
                case 1; case 4; case 7; case 13; case 16; case 18; case 20; case 23; case 26;  case 27; case 29;
                return 2;
                break;
                case 2; case 5; case 8; case 9; case 11; case 14; case 19; case 21; case 24; case 28; case 30;
                return 3;
                break;
            }
        }

    }

    private function cities($city) {

        //for ZAGS
        // if(isset($city[5])) $city = $this->btw($city[5], '>', '<');
        // if(isset($city[4])) $city = $this->btw($city[4], '>', '<');
        // if(isset($city[3])) $city = $this->btw($city[3], '>', '<');
        //
        
        $cities = json_decode(file_get_contents('/Volumes/Macintosh HD/Users/slavitto/Sites/budetsvadba/resources/assets/js/components/data/cities.json'));
        
        // $city_recoded = preg_replace('/[^0123456789-а-ячстюрцущфхыьА-Я\s.]/', '', mb_convert_encoding($this->recode($city), 'utf-8'));
        // echo $city_recoded.$cities[0].'<br>';
        return array_search($city, $cities);
    }

    private function metro($metro) {

        //for ZAGS
        if(isset($metro[6])) $metro = $this->btw($metro[5], '>', '<');
        if(isset($metro[5])) $metro = $this->btw($metro[4], '>', '<');
        if(isset($metro[4])) $metro = $this->btw($metro[3], '>', '<');
        //
        
        $metros = json_decode(file_get_contents('/Volumes/Macintosh HD/Users/slavitto/Sites/budetsvadba/resources/assets/js/components/data/metro.json'));
        
        $metro_recoded = preg_replace('/[^0123456789-а-ячстюрцущфхыьА-Я\s.]/', '', mb_convert_encoding($this->recode($metro), 'utf-8'));
        var_dump($metro_recoded);
        return array_search($metro_recoded, $metros[0]);
    }

    private function preg($data) {
        foreach($data as $i => $val) {
            if($i === 1) return $val;
        }
    }

    private function phone($data) {
        //COMPANY
        // $a = $this->btw($data, '(', '</');
        // $b = $this->btw($data, '8-', '</');
        // $c = $this->btw($data, '+7', '</');

        // if($a) return '('.$a;
        // if($b) return '8-'.$b;
        // if($c) return '+7'.$c;

        //ZAGS
        for($x = 2; $x <= 9; $x++) {
            $tel = $this->btw($data, '>'.$x, '</');
            if($tel) return $x.$tel;
        }

    }

    private function mStatus($data) {
        $status = ['Женат', 'Замужем', 'В поиске', 'В серьезных отношениях', 'Не замужем', 'Не женат', 'Все непросто'];
        $sNum = array_search($data, $status);

        // echo $data.$status[3].'<br>';

        switch($sNum) {
            case 1; case 0;
            return 1;
            break;

            case 3;
            return 2;
            break;

            case 2; case 4; case 5; case 6;
            return 3;
            break;

            default: return 0;
        }

    }

    // private function tags($tags) {
    //     $tags = explode(',', $tags);
    //     $ids = '';
    //     foreach($tags as $tag) {
    //         $name = mb_convert_encoding($this->recode($this->btw($tag, '>', '<')), 'utf-8');
    //         $id = Category::where('name', $name)->first()->id;
    //         $ids .= $id.'|';
    //     }
    //     return $ids;
    // }
    
    // private function boxData($txt, $type = 0) {

    //     // $type == 1 ? $data_arr = ['Все', 'Гости', 'Родственники', 'Молодые', 'Для девочек', 'Для мальчиков'] : 
    //     // $type == 2 ? $data_arr = ['Любое','Дома','В ресторане, кафе','На открытом воздухе','На теплоходе','Экзотические места'] :
    //     switch($type) {
    //         case 1: $data_arr = ['За молодых','За родителей невесты','За родителей жениха','За друзей','общие','Жениху','Невесте','"любленным'];
    //         break;
    //         case 2: $data_arr = ['От тещи','От тестя','От родственников','От тамады'];
    //         break;
    //         default: $data_arr = ['проза','стих'];
    //     } 

    //     // $data_arr = ['Любое', '1-3', '3-5', '5 и более', 'все'];

    //     $txt_recoded = preg_replace('/[^0-9-а-ячстюрцущфхыьА-Я" "]/', '', mb_convert_encoding($this->recode($txt), 'utf-8'));

    //     // echo $txt_recoded.$data_arr[1].'<br>';

    //     // if($type == 0) {
    //     //     switch($txt_recoded) {
    //     //         case 'менее 5': return 2;
    //     //         case '5-10'; case '10-20'; return 3;
    //     //         case '20-50'; case '50 и более': return 4;
    //     //         default: return 0;
    //     //     }
    //     // }
    //     return array_search($txt_recoded, $data_arr);
    // }

    // --------- FOR ARTICLES
    private function cats($cat, $type) 
    {
        $data = json_decode(file_get_contents('/Users/slavitto/Sites/budetsvadba/resources/assets/js/components/data/categories.json'));
        if($type == 1) {

            for($i=0; $i<count($data); $i++ ) {
                
                if(mb_convert_encoding($data[$i]->name, 'windows-1251') == trim($cat)) {

                    return $i;
                }
            }
        } else if ($type == 2) {
            $data_sub = array_merge($data[0]->sub, $data[1]->sub, $data[2]->sub, $data[3]->sub, $data[4]->sub);

            for($i=0; $i<count($data_sub); $i++ ) {

                if(mb_convert_encoding($data_sub[$i], 'windows-1251') == trim($cat)) {

                    return $i;
                } 
            }
        } else if ($type == 3) {
            $data_sub_extra = array_merge($data[0]->sub_extra, $data[5]->sub_extra, $data[6]->sub_extra, $data[7]->sub_extra, $data[8]->sub_extra);

            for($i=0; $i<count($data_sub_extra); $i++ ) {

                if(mb_convert_encoding($data_sub_extra[$i], 'windows-1251') == trim(substr($cat,0,strlen($cat)-4))) {

                    return $i;
                } 
            }
        }
    }  

    //ARTICLE, TOASTS, CONTESTS
    // private function update($data)
    // {
    //     if(!$data['body']) return false;

    //     $article = new article();
    //     // $article = Article::where('title', preg_replace('" \?"', ' —', mb_convert_encoding($this->recode($data['title']), 'utf-8')))->first();


    //     $article->author_id = $data['author_id'];
    //     // $article->title = preg_replace('" \?"', ' —', mb_convert_encoding($this->recode($data['title']), 'utf-8'));
    //     // $article->url = $data['url'];
    //     // $article->description = preg_replace('" \?"', ' —', mb_convert_encoding($this->recode($data['description']), 'utf-8'));
    //     $article->body = preg_replace('" \?"', ' —', mb_convert_encoding($this->recode($data['body']), 'utf-8'));
    //     $article->category = $data['category'];
    //     $article->sub_category = $data['sub_category'];
    //     // $article->extra_category = $data['extra_category'];
    //     // $article->guest_count = $data['guest_count'];
    //     // $article->place = $data['place'];
    //     // $article->players = $data['players'];
    //     // $article->thumb = $data['thumb'];
    //     $article->to_whom = $data['to_whom'];
    //     $article->from_who = $data['from_who'];
    //     $article->genre = $data['genre'];


    //     print_r(mb_convert_encoding($this->recode($data), 'utf-8'));
    //     // echo ord('«»–').'<br>';

    //     // if($article->save()) {
    //     //     return new ArticleResource($article);
    //     //     echo $article->title.'<br>';
    //     // }

    // }


    //COMPANY AND ZAGS AND USER
    private function update($data)
    {
        // if(!$data['url'] || preg_match('/html/', $data['url'])) return;
        // if(!file_exists('/Volumes/Macintosh HD/Users/slavitto/Documents/_projects/_www/budetsvadba.ru'.$data['url'])) return;

        $media = new Media();
        // $company = company::where('title', preg_replace('" \?"', ' —', mb_convert_encoding($this->recode($data['title']), 'utf-8')))->first();


        // $company->author_id = $data['author_id'];
        $media->url = $data['url'];
        $media->album_id = $data['album_id'];
        $media->album_name = $data['album_name'];
        $media->description = $data['description'];
        // $media->created_at = date('Y-m-d', strtotime(str_replace('-', '/', $data['created_at'])));
        $media->user_id = Company::where('url', $data['user_login'])->first()['id'];
        // $user->url = $data['url'];
        // $user->img_url = '/images/users/main_photos/'.$data['img_url'];
        // $user->name =  trim(strip_tags($data['name']));
        // $user->gender = substr($data['gender'], 15, 14) === 'Женский' ? 0 : 1;
        // $user->martial_status = $this->mStatus(substr($data['martial_status'], 0, -6));
        // $date = substr($data['date_of_birth'], 15, 10);
        // if($date) $user->date_of_birth = date('Y-m-d', strtotime(str_replace('-', '/', $date)));

        // $date_w = substr($data['date_wedding'], 15, 10);
        // if($date_w) $user->date_wedding = date('Y-m-d', strtotime(str_replace('-', '/', $date_w)));

        // $user->company_id = Company::where('url', $data['company'])->first()['id'];

        // $user->interests_list = substr($data['interests'], 1);

        // $company->description = preg_replace('" \?"', ' —', mb_convert_encoding($this->recode($data['description']), 'utf-8'));
        // $company->about = preg_replace('" \?"', ' —', mb_convert_encoding($this->recode($data['about']), 'utf-8'));
        // $company->category = $data['category'];
        // $company->sub_category = $data['sub_category'];
        // $company->sphere = $data['extra_category'];
        // $company->phone = preg_replace('/[^0-9-+()\s]/','',$data['phone']);
        // $company->url = $data['url'];
        // $company->www = preg_replace('/[^a-z-.A-Z0-9]/','',$data['www']);
        // $user->city = $this->cities(substr($data['city'], 0, -6));
        // $user->email = rand(1,100000000).'@budetsvadba.ru';
        // $company->price_category = $data['price_category'];
        // $company->address = mb_convert_encoding($this->recode($data['address']), 'utf-8');

        print_r($data);
        // echo ord('«»–').'<br>';
        // if(!$media->user_id) return;

        // if($media->save()) {
        //     // return new CompanyResource($company);
        //     echo $media->url.'<br>';
        // }

    }

    private function recode($txt)
    {
        $in_arr = array (
            chr(208), chr(192), chr(193), chr(194),
            chr(195), chr(196), chr(197), chr(168),
            chr(198), chr(199), chr(200), chr(201),
            chr(202), chr(203), chr(204), chr(205),
            chr(206), chr(207), chr(209), chr(210),
            chr(211), chr(212), chr(213), chr(214),
            chr(215), chr(216), chr(217), chr(218),
            chr(219), chr(220), chr(221), chr(222),
            chr(223), chr(224), chr(225), chr(226),
            chr(227), chr(228), chr(229), chr(184),
            chr(230), chr(231), chr(232), chr(233),
            chr(234), chr(235), chr(236), chr(237),
            chr(238), chr(239), chr(240), chr(241),
            chr(242), chr(243), chr(244), chr(245),
            chr(246), chr(247), chr(248), chr(249),
            chr(250), chr(251), chr(252), chr(253),
            chr(254), chr(255)
        );   
     
        $out_arr = array (
            chr(208).chr(160), chr(208).chr(144), chr(208).chr(145),
            chr(208).chr(146), chr(208).chr(147), chr(208).chr(148),
            chr(208).chr(149), chr(208).chr(129), chr(208).chr(150),
            chr(208).chr(151), chr(208).chr(152), chr(208).chr(153),
            chr(208).chr(154), chr(208).chr(155), chr(208).chr(156),
            chr(208).chr(157), chr(208).chr(158), chr(208).chr(159),
            chr(208).chr(161), chr(208).chr(162), chr(208).chr(163),
            chr(208).chr(164), chr(208).chr(165), chr(208).chr(166),
            chr(208).chr(167), chr(208).chr(168), chr(208).chr(169),
            chr(208).chr(170), chr(208).chr(171), chr(208).chr(172),
            chr(208).chr(173), chr(208).chr(174), chr(208).chr(175),
            chr(208).chr(176), chr(208).chr(177), chr(208).chr(178),
            chr(208).chr(179), chr(208).chr(180), chr(208).chr(181),
            chr(209).chr(145), chr(208).chr(182), chr(208).chr(183),
            chr(208).chr(184), chr(208).chr(185), chr(208).chr(186),
            chr(208).chr(187), chr(208).chr(188), chr(208).chr(189),
            chr(208).chr(190), chr(208).chr(191), chr(209).chr(128),
            chr(209).chr(129), chr(209).chr(130), chr(209).chr(131),
            chr(209).chr(132), chr(209).chr(133), chr(209).chr(134),
            chr(209).chr(135), chr(209).chr(136), chr(209).chr(137),
            chr(209).chr(138), chr(209).chr(139), chr(209).chr(140),
            chr(209).chr(141), chr(209).chr(142), chr(209).chr(143)
        );  

        $txt = str_replace($in_arr,$out_arr,$txt);

        return $txt;
    }

}

