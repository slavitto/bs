<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Article;
use JavaScript;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $editable = Auth()->id() == 1 ? 1 : 0;

        /* Theme of the Day */
        $newestAricle = Article::orderBy('updated_at', 'desc')->first();

        // dd(strtotime($newestAricle->updated_at)); //1537891482
        // dd(strtotime("now") - 60 * 60 * 14); //1536861670

        if(strtotime($newestAricle->updated_at) > strtotime("now") - 60 * 60 * 12) { // last 12 hrs (GMT?)

            $first_page_article = $newestAricle; // if there is a new article
        } else {

            $articles_count = 734; // All articles except "Wedding stories" category
            $bs_timestamp = ceil(strtotime("now") / (60 * 60 * 24)) - 18152; // 12sep2019-743 
            
            $theme_of_the_day_offset = intval($bs_timestamp + $articles_count * round($bs_timestamp / $articles_count) + 1); // loop (seems not working)
            $first_page_article = Article::whereNotIn('category', [7])->offset($theme_of_the_day_offset)->limit(1)->first();

            if(!$first_page_article) $first_page_article = Article::where('id', 100)->first(); // if the counter is over
        }        
        /*  */

        JavaScript::put([
            'article_id' => $first_page_article->id,
            'category_id' => $first_page_article->category,
            'editable' => $editable
        ]);

        return view('home');
    }
}
