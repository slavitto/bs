@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Почитать</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if($edit == true)
                        <article-edit-component></article-edit-component>
                    @else
                        <article-view-component></article-view-component>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-2 p-0">
            <article-right-component></article-right-component>
        </div>
    </div>
</div>
@endsection
