@extends('layouts.app')

@section('content')
<!-- @if (Route::has('login'))
    <div class="top-right links">
        @auth
            <a href="{{ url('/home') }}">Home</a>
        @else
            <a href="{{ route('login') }}">Login</a>
            <a href="{{ route('register') }}">Register</a>
        @endauth
    </div>
@endif -->
<div class="content">
    <div class="title m-b-md">
        BudetSvadba.Ru
    </div>
</div>
<div class="container">
    <div class="col-md-8 m-auto">
        <div class="row menu-welcome">
            <div class="col-md-6"><a href="{{ route('readup') }}">Почитать</a></div>
            <div class="col-md-6"><a href="{{ route('lookup') }}">Посмотреть</a></div>
            <div class="col-md-6"><a href="{{ route('talkup') }}">Пообщаться</a></div>
            <div class="col-md-6"><a href="{{ route('pickup') }}">Подобрать</a></div>
        </div>
    </div>
</div>
@endsection