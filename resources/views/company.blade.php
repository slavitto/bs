@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Компания') }}
                </div>
                <div class="card-body">
                    @if($edit == true)
                        <company-edit-component></company-edit-component>
                    @else
                        <company-view-component></company-view-component>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-2 p-0">
            <company-right-component></company-right-component>
        </div>
    </div>
</div>
@endsection
