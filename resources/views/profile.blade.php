@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Профиль') }}
                </div>
                <div class="card-body">
                    @if($edit == true)
                        <profile-edit-component></profile-edit-component>
                    @else
                        <profile-view-component></profile-view-component>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
