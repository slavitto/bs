@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Медиа') }}
                </div>
                <div class="card-body">
                    @if($edit == true)
                        <media-edit-component></media-edit-component>
                    @else
                        <media-view-component></media-view-component>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
