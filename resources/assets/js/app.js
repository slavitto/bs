
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Datepicker from 'vuejs-datepicker'
import { VueEditor, Quill } from 'vue2-editor'

// Vue.config.productionTip = false; // DEV MODE warning removing //

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('menu-component', require('./components/MenuComponent.vue'));

Vue.component('lookup-component', require('./components/LookupComponent.vue'));
Vue.component('talkup-component', require('./components/TalkupComponent.vue'));
Vue.component('pickup-component', require('./components/PickupComponent.vue'));
Vue.component('readup-component', require('./components/ReadupComponent.vue'));

Vue.component('profile-edit-component', require('./components/ProfileEditComponent.vue'));
Vue.component('profile-view-component', require('./components/ProfileViewComponent.vue'));
Vue.component('company-edit-component', require('./components/CompanyEditComponent.vue'));
Vue.component('company-view-component', require('./components/CompanyViewComponent.vue'));
Vue.component('article-edit-component', require('./components/ArticleEditComponent.vue'));
Vue.component('article-view-component', require('./components/ArticleViewComponent.vue'));
Vue.component('media-edit-component', require('./components/MediaEditComponent.vue'));
Vue.component('media-view-component', require('./components/MediaViewComponent.vue'));

Vue.component('home-component', require('./components/HomeComponent.vue'));

Vue.component('article-right-component', require('./components/ArticleRightComponent.vue'));
Vue.component('company-right-component', require('./components/CompanyRightComponent.vue'));

Vue.component('payout-component', require('./components/PayoutComponent.vue'));

Vue.component('datepicker', Datepicker);
Vue.component('VueEditor', VueEditor);


const 	app = new Vue({
    		el: '#app'
		});

export const DESC = 'Свадебный сайт не похожий на другие, подготовка к свадьбе, свадебные истории, полезные адреса, тесты, фотоальбомы молодоженов, видео и многое другое Вы найдете на The-Svadba.Ru';

