webpackJsonp([1],{

/***/ 43:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(44)
/* template */
var __vue_template__ = __webpack_require__(45)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/ProfileComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1891bcea", Component.options)
  } else {
    hotAPI.reload("data-v-1891bcea", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      profile: {
        // name: '',
        // status: '',
        // about: '',
        // gender: '',
        // date_of_birth: '',
        // martial_status: '',
        // date_wedding: '',
        // country: '',
        // city: '',
        // company: '',
        // phone: '',
        // email: '',
        // password: '',
        // interests_list: '',
        // img_url: ''
      },
      interests: [],
      uploadText: 'Обновить фото',
      disabled: ''
    };
  },
  created: function created() {
    this.fetchProfile();
  },


  methods: {
    fetchProfile: function fetchProfile() {
      var _this = this;

      fetch('/api/user/' + login).then(function (res) {
        return res.json();
      }).then(function (res) {
        var vm = _this;
        vm.profile = res.data;
        vm.profile.login = login;
        if (vm.profile.img_url === '') {
          vm.profile.img_url = '/images/dummy.png';
          vm.uploadText = 'Загрузить фото';

          // vm.disabled = 'form-disabled'; // -- DISABLE FORM
        }
      }).catch(function (err) {
        return console.log(err);
      });
    },
    formsubmit: function formsubmit() {
      var _this2 = this;

      this.profile.interests_list = this.interests.join(', ') || 'Свадебная тематика';

      // axios.put('/api/user/',{ profile: this.profile })
      //     .then(response => {
      //       console.log(response)
      //     })
      //     .catch(err => console.log(err));

      fetch('/api/user/', {
        method: 'put',
        body: JSON.stringify(this.profile),
        headers: {
          'content-type': 'application/json'
        }
      })
      // .then(res => res.JSON())
      .then(function (data) {
        _this2.fetchProfile();
      }).catch(function (err) {
        return console.log(err);
      });
    },
    onFileChange: function onFileChange(e) {

      var files = e.target.files || e.dataTransfer.files,
          reader = new FileReader(),
          vm = this;

      if (!files.length) return;

      reader.onload = function (e) {
        axios.post('/api/upload', { image: e.target.result }).then(function (response) {
          vm.profile.img_url = '/images/' + response.data.file;
          vm.uploadText = "Обновить фото";
        }).catch(function (err) {
          return console.log(err);
        });
      };
      reader.readAsDataURL(files[0]);
    }
  }
});

/***/ }),

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "profile-login" }, [
      _vm._v(_vm._s(_vm.profile.login))
    ]),
    _vm._v(" "),
    _c(
      "form",
      { class: _vm.disabled, attrs: { enctype: "multipart/form-data" } },
      [
        _c("div", { staticClass: "form-row mb-3 ml-1 pb-4 border-bottom" }, [
          _c("div", { staticClass: "col-md-4" }, [
            _c("div", { staticClass: "border rounded mt-4 profile-img" }, [
              _c("img", {
                staticClass: "img-fluid",
                attrs: { src: _vm.profile.img_url, alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("label", { staticClass: "btn btn-light col-md-12 mt-2" }, [
              _vm._v(_vm._s(_vm.uploadText)),
              _c("input", {
                attrs: { type: "file", hidden: "" },
                on: { change: _vm.onFileChange }
              })
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-7 ml-5" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "name" } }, [
                _vm._v("Отображаемое имя")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.profile.name,
                    expression: "profile.name"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "name", placeholder: "" },
                domProps: { value: _vm.profile.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.profile, "name", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "status" } }, [_vm._v("Статус")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.profile.status,
                    expression: "profile.status"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "status", placeholder: "" },
                domProps: { value: _vm.profile.status },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.profile, "status", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "about" } }, [
                _vm._v("Коротко о себе")
              ]),
              _vm._v(" "),
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.profile.about,
                    expression: "profile.about"
                  }
                ],
                staticClass: "form-control",
                attrs: { id: "about", placeholder: "" },
                domProps: { value: _vm.profile.about },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.profile, "about", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-row", attrs: { id: "interests" } }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "interests_list" } }, [
                  _vm._v("Интересы")
                ]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.interests,
                      expression: "interests"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    id: "interests_list",
                    placeholder: ""
                  },
                  domProps: { value: _vm.interests },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.interests = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "form-check m-2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.interests,
                        expression: "interests"
                      }
                    ],
                    staticClass: "form-check-input",
                    attrs: { type: "checkbox", value: "Одежда", id: "check1" },
                    domProps: {
                      checked: Array.isArray(_vm.interests)
                        ? _vm._i(_vm.interests, "Одежда") > -1
                        : _vm.interests
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.interests,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "Одежда",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.interests = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.interests = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.interests = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "form-check-label",
                      attrs: { for: "check1" }
                    },
                    [_vm._v("\n                  Одежда\n                ")]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-check m-2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.interests,
                        expression: "interests"
                      }
                    ],
                    staticClass: "form-check-input",
                    attrs: { type: "checkbox", value: "Цветы", id: "check2" },
                    domProps: {
                      checked: Array.isArray(_vm.interests)
                        ? _vm._i(_vm.interests, "Цветы") > -1
                        : _vm.interests
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.interests,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "Цветы",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.interests = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.interests = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.interests = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "form-check-label",
                      attrs: { for: "check2" }
                    },
                    [_vm._v("\n                  Цветы\n                ")]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-check m-2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.interests,
                        expression: "interests"
                      }
                    ],
                    staticClass: "form-check-input",
                    attrs: { type: "checkbox", value: "Музыка", id: "check3" },
                    domProps: {
                      checked: Array.isArray(_vm.interests)
                        ? _vm._i(_vm.interests, "Музыка") > -1
                        : _vm.interests
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.interests,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "Музыка",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.interests = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.interests = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.interests = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "form-check-label",
                      attrs: { for: "check3" }
                    },
                    [_vm._v("\n                  Музыка\n                ")]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-check m-2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.interests,
                        expression: "interests"
                      }
                    ],
                    staticClass: "form-check-input",
                    attrs: {
                      type: "checkbox",
                      value: "Выключен",
                      id: "check4",
                      disabled: ""
                    },
                    domProps: {
                      checked: Array.isArray(_vm.interests)
                        ? _vm._i(_vm.interests, "Выключен") > -1
                        : _vm.interests
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.interests,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "Выключен",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.interests = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.interests = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.interests = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "form-check-label",
                      attrs: { for: "check4" }
                    },
                    [_vm._v("\n                  Выключен\n                ")]
                  )
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-row" }, [
          _c("div", { staticClass: "form-group col-md-1" }, [
            _c("label", { attrs: { for: "gender" } }, [_vm._v("Пол:")]),
            _vm._v(" "),
            _c(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.profile.gender,
                    expression: "profile.gender"
                  }
                ],
                staticClass: "form-control",
                on: {
                  change: function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.$set(
                      _vm.profile,
                      "gender",
                      $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                    )
                  }
                }
              },
              [
                _c("option", [_vm._v("Женский")]),
                _vm._v(" "),
                _c("option", [_vm._v("Мужской")])
              ]
            )
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "form-group col-md-3" },
            [
              _c("label", { attrs: { for: "date_of_birth" } }, [
                _vm._v("Дата рождения:")
              ]),
              _vm._v(" "),
              _c("datepicker"),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.profile.date_of_birth,
                    expression: "profile.date_of_birth"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text" },
                domProps: { value: _vm.profile.date_of_birth },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.profile, "date_of_birth", $event.target.value)
                  }
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "form-group col-md-3" }, [
            _c("label", { attrs: { for: "martial_status" } }, [
              _vm._v("Семейное положение:")
            ]),
            _vm._v(" "),
            _c(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.profile.martial_status,
                    expression: "profile.martial_status"
                  }
                ],
                staticClass: "form-control",
                on: {
                  change: function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.$set(
                      _vm.profile,
                      "martial_status",
                      $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                    )
                  }
                }
              },
              [
                _c("option", [_vm._v("В браке")]),
                _vm._v(" "),
                _c("option", [_vm._v("В отношениях")]),
                _vm._v(" "),
                _c("option", [_vm._v("В поиске")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group col-md-3" }, [
            _c("label", { attrs: { for: "profile.date_wedding" } }, [
              _vm._v("Дата свадьбы:")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.profile.date_wedding,
                  expression: "profile.date_wedding"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "date_wedding" },
              domProps: { value: _vm.profile.date_wedding },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.profile, "date_wedding", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-row" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "phone" } }, [_vm._v("Телефон")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.profile.phone,
                  expression: "profile.phone"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "phone", placeholder: "+" },
              domProps: { value: _vm.profile.phone },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.profile, "phone", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group col-md-4" }, [
            _c("label", { attrs: { for: "city" } }, [_vm._v("Город")]),
            _vm._v(" "),
            _c(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.profile.city,
                    expression: "profile.city"
                  }
                ],
                staticClass: "form-control",
                attrs: { id: "city" },
                on: {
                  change: function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.$set(
                      _vm.profile,
                      "city",
                      $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                    )
                  }
                }
              },
              [
                _c("option", [_vm._v("Москва")]),
                _vm._v(" "),
                _c("option", [_vm._v("Санкт-Петербург")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group col-md-4" }, [
            _c("label", { attrs: { for: "company" } }, [_vm._v("Компания")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.profile.company,
                  expression: "profile.company"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "company" },
              domProps: { value: _vm.profile.company },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.profile, "company", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-row" }, [
          _c("div", { staticClass: "form-group col-md-6" }, [
            _c("label", { attrs: { for: "email" } }, [_vm._v("Email")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.profile.email,
                  expression: "profile.email"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "email", id: "email", placeholder: "" },
              domProps: { value: _vm.profile.email },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.profile, "email", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group col-md-6" }, [
            _c("label", { attrs: { for: "password" } }, [_vm._v("Пароль")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.profile.password,
                  expression: "profile.password"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "password", id: "password", placeholder: "" },
              domProps: { value: _vm.profile.password },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.profile, "password", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "btn btn-primary",
            on: {
              click: function($event) {
                _vm.formsubmit()
              }
            }
          },
          [_vm._v("Сохранить")]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "form-check" }, [
        _c("input", {
          staticClass: "form-check-input",
          attrs: { type: "checkbox", id: "gridCheck" }
        }),
        _vm._v(" "),
        _c(
          "label",
          { staticClass: "form-check-label", attrs: { for: "gridCheck" } },
          [
            _vm._v(
              "\n            Подтверждаю согласие на обработку персональных данных\n          "
            )
          ]
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1891bcea", module.exports)
  }
}

/***/ }),

/***/ 55:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 55;

/***/ })

});