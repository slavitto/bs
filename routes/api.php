<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('users', 'TalkupController@show');
Route::get('user/{login}', 'ProfileController@show');
Route::put('user', 'ProfileController@update');

Route::get('companies', 'PickupController@show');
Route::get('company/{id}', 'CompanyController@show');
Route::match(['put', 'post'], 'company', 'CompanyController@update');

Route::get('allmedia/{id?}', 'MediaController@showAll');
Route::get('media/{id}', 'MediaController@show');
Route::match(['put', 'post'], 'media', 'MediaController@update');
Route::post('upload', 'MediaController@upload');

Route::get('articles', 'ReadupController@show');
Route::get('article/{id}', 'ArticleController@show');
Route::match(['put', 'post'], 'article', 'ArticleController@update');

Route::get('category/{id}', 'CategoryController@show');
Route::get('categories/{parent_id?}', 'CategoryController@showAll');