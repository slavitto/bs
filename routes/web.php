<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/sitemap', 'SitemapController@index')->name('sitemap');

Route::get('/payout', 'PayoutController@index')->name('payout');

// Route::get('/parser/{dir?}/{subdir?}', 'ParseController@index')->name('parser');

Route::get('/readup.html', 'ReadupController@index')->name('readup');
Route::get('/lookup.html', 'LookupController@index')->name('lookup');
Route::get('/talkup.html', 'TalkupController@index')->name('talkup');
Route::get('/pickup.html', 'PickupController@index')->name('pickup');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/{login}.html', 'ProfileController@index')->name('profile');
Route::get('/user/{id}', 'ProfileController@edit')->name('profile');
Route::get('/company/{id?}', 'CompanyController@index')->name('company');
Route::get('/article/{id?}', 'ArticleController@edit')->name('article');
Route::get('/media/{id}', 'MediaController@edit')->name('media');
Route::get('/{owner}/ActionShow/{path}/{media}.html', 'MediaController@index')->name('media');

Route::get('/{path1?}/{path2?}/{path3?}', 'MainController@index')->name('main');